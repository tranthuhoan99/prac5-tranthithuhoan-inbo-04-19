package com.tranthuhoan.Prac5;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class Prac5ApplicationTests {

	//From K
	@Test
	public void testCase1() {
		Convert convert = new Convert(1, "K", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 K = -272.15 C");
	}

	@Test
	public void testCase2() {
		Convert convert = new Convert(1, "K", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 K = -457.87 F");
	}
	@Test
	public void testCase3() {
		Convert convert = new Convert(1, "K", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 K = 1 K");
	}

	//From C
	@Test
	public void testCase4() {
		Convert convert = new Convert(1, "C", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 C = 274.15 K");
	}

	@Test
	public void testCase5() {
		Convert convert = new Convert(1, "C", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 C = 33.8 F");
	}
	@Test
	public void testCase6() {
		Convert convert = new Convert(1, "C", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 C = 1 C");

	}
	//From F
	@Test
	public void testCase7() {
		Convert convert = new Convert(1, "F", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 F = -17.22 C");

	}
	@Test
	public void testCase8() {
		Convert convert = new Convert(1, "F", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 F = 255.93 K");
	}

	@Test
	public void testCase9() {
		Convert convert = new Convert(1, "F", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("1 F = 1 F");
	}
	@Test
	public void testCase10() {
		Convert convert = new Convert(1, "T", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase11() {
		Convert convert = new Convert(1, "C", "L");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase12() {
		Convert convert = new Convert("pr5", "C", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Value is a number, please try again.");
	}

	@Test
	public void testCase13() {
		Convert convert = new Convert(1, "T", "L");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase14() {
		Convert convert = new Convert("pr5", "L", "T");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Value is a number, please try again.");
	}
	//From K
	@Test
	public void testCase15() {
		Convert convert = new Convert(5, "K", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 K = -268.15 C");
	}

	@Test
	public void testCase16() {
		Convert convert = new Convert(5, "K", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 K = -450.67 F");
	}
	@Test
	public void testCase17() {
		Convert convert = new Convert(5, "K", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 K = 5 K");
	}
	//From C
	@Test
	public void testCase18() {
		Convert convert = new Convert(5, "C", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 C = 278.15 K");
	}

	@Test
	public void testCase19() {
		Convert convert = new Convert(5, "C", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 C = 41 F");
	}
	@Test
	public void testCase20() {
		Convert convert = new Convert(5, "C", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 C = 5 C");

	}
	//From F
	@Test
	public void testCase21() {
		Convert convert = new Convert(5, "F", "C");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 F = -15 C");

	}
	@Test
	public void testCase22() {
		Convert convert = new Convert(5, "F", "K");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 F = 258.15 K");
	}

	@Test
	public void testCase23() {
		Convert convert = new Convert(5, "F", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("5 F = 5 F");
	}
	@Test
	public void testCase24() {
		Convert convert = new Convert(5, "T", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase25() {
		Convert convert = new Convert(5, "C", "L");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase26() {
		Convert convert = new Convert("pr4", "C", "F");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Value is a number, please try again.");
	}

	@Test
	public void testCase27() {
		Convert convert = new Convert(1, "N", "B");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Unit must be one of(C, F, K), please try again.");
	}

	@Test
	public void testCase28() {
		Convert convert = new Convert("hen", "P", "T");
		String output = convert.conv();
		convert.writeFile("result.txt", output + "\n");
		assertThat(output).isEqualTo("Value is a number, please try again.");
	}

}
