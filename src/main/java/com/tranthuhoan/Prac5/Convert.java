package com.tranthuhoan.Prac5;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
/**
 Create Class Convert имеет 4 свойства:
 value - число: значение - это входное число;
 from - string: текущая единица измерения температуры ;
 to - строка: единица измерения после преобразования;
 check: проверьте, является ли входное значение числом или строкой
 */
public class Convert {
    private double value;
    private String from;
    private String to;

    private boolean check;

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
    //Конструктор с 3 аргументами (1 число и 2 строки)
    public Convert(double value, String from, String to) {
        this.check = true;
        this.value = value;
        this.from = from;
        this.to = to;
    }
    //Конструктор с 3 аргументами (3 строки)
    public Convert(String value, String from, String to) {
        this.check = false;
        this.from = from;
        this.to = to;
    }
    //Конструктор по умолчанию (без аргументов)
    public Convert() {

    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    @Override
    public String toString() {
        return "Convert{" +
                "value=" + value +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                '}';
    }
//Формат числа – двойной
//Ex 1.0 -> 1
    public static String fmt(double d)
    {
        d = (double)Math.round(d*100)/100;
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
    //Запишите результат в файл
    public void writeFile(String path, String output) {
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            File file = new File(path);

            if (!file.exists()) {
                file.createNewFile();
            }

            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(output);
            System.out.println("Success...");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public String conv() {
        String[] unitOfLength = {"C", "F", "K" };
        double valueTemp = 0;

        if(!check) {
            return  "Value is a number, please try again.";
        }
        else if (!Arrays.asList(unitOfLength).contains(this.from) || !Arrays.asList(unitOfLength).contains(this.to)) {
            return "Unit must be one of(C, F, K), please try again.";
        } else {
            if (from.equals("C")) {
                if (to.equals("F"))
                    valueTemp =  value * ((double)9/5) + 32;
                else if (to.equals("K"))
                    valueTemp = value + 273.15;
                else if (to.equals("C"))
                    valueTemp = value;

            } else if (from.equals("F")) {
                if (to.equals("C"))
                    valueTemp = ( value - 32) * ((double)5/9);
                else if (to.equals("K"))
                    valueTemp = ( value - 32) * ((double)5/9) + 273.15;
                else if (to.equals("F"))
                    valueTemp = value;

            } else if (from.equals("K")) {
                if (to.equals("C"))
                    valueTemp = value - 273.15;
                else if (to.equals("F"))
                    valueTemp = ((value - 273.15) * ((double)9/5)) + 32;
                else if (to.equals("K"))
                    valueTemp = value;
            }

            return fmt(value) + " " + from + " = " + fmt(valueTemp) + " " + to;
        }
    }
}
