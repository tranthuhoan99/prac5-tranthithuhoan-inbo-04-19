package com.tranthuhoan.Prac5;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

@SpringBootApplication
public class Prac5Application {

static String[] unitOfLength = {"K", "F", "C"};

	public static Scanner sc = new Scanner(System.in);
	//Проверить, является ли строка числом
	public static boolean isNumber(String s) {
		for (int i = 0; i < s.length(); i++)
			if (!Character.isDigit(s.charAt(i)))
				return false;
		return true;
	}
	//Функция ввода
	public static boolean input(Convert convert) {
		String valueTemp, from, to;
		int value = 0;

		//value
		System.out.print("Enter the value to convert: ");
		valueTemp = sc.nextLine();
		if (isNumber(valueTemp))
			value = Integer.parseInt(valueTemp);
		else {
			System.out.println("Value is a number, please try again.");

			return false;
		}

		//from
		System.out.print("Enter unit of the current value(K, F, C): ");
		from = sc.nextLine();
		if (!Arrays.asList(unitOfLength).contains(from)) {
			System.out.println("Unit of the current value must be one of(K, F, C, please try again.");
			return false;
		}

		//to
		System.out.print("Enter unit to be converted(K, F, C): ");
		to = sc.nextLine();
		if (!Arrays.asList(unitOfLength).contains(to)) {
			System.out.println("Unit to be converted must be one of(K, F, C), please try again.");
			return false;
		}

		convert.setCheck(true);
		convert.setValue(value);
		convert.setFrom(from);
		convert.setTo(to);
		return true;

	}
	//Запишите результат в файл
	public static void writeFile(String path, String output) {
		BufferedWriter bw = null;
		FileWriter fw = null;

		try {

			File file = new File(path);

			if (!file.exists()) {
				file.createNewFile();
			}

			fw = new FileWriter(file.getAbsoluteFile(), true);
			bw = new BufferedWriter(fw);
			bw.write(output);
			System.out.println("\nSuccess...");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (bw != null)
					bw.close();
				if (fw != null)
					fw.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	//Основная функция запускает программу
	public static void main(String[] args) {
		Convert convert = new Convert();
		String output;
		boolean flag = true;
		do {

			if (input(convert)) {
				output = convert.conv();
				System.out.print(output);

				writeFile("result.txt", output);

				System.out.print("Do you want continue(Y/N): ");
				char check = sc.next().charAt(0);
				if (check == 'n' || check == 'N') flag = false;
				sc.nextLine();

			}
		} while (flag);

	}
}
